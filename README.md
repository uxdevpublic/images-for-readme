**FRONT END DEVELOPMENT**
=======================

IMPLEMENTATION
===============


TABLE OF CONTENTS
======




**1. INTRODUCTION**
---
**2. TOOLS**
---
**3. FOLDER STRUCTURE**
---
**4. THEME IDENTIFICATION	**
---
**5. CONFIGURATION FILE**
---
**6. COMPILATION PROCESS**
---
**7. BUG FIXING PROCESS**
---
**8. NEW FLOW PROCESS**
---
**9. CONCLUSION**
---



________


**1.	Introduction**
---

A Few years ago, front end development (FE) was heavily done by graphic designers, whom built websites with techniques that nowadays are considered obsoletes. Image and text manipulation was done through visual tools to generate markup and style sheets. Those tools used to produce folders that later were copied to a server.
Due users demands, technological advances and development optimization techniques, FE has suffer mayor changes that require developers to specialize in several languages and tools.

Currently, FE development require characteristics that a few years back weren’t even considered. Browser support now dictate the techniques to be used. Also, screen resolution from several devices needs to be considered, so the website can adapt to an specific resolution. Due loading time can be the difference between a potencial client, optimization its another heavy aspect to consider.

Recently, tools have emerged to help developer build web aplications in a shorter time. Tools like Bootstraps for quick prototyping, GRUNT for task automation and LESS for processing stylesheets have allowed a controlled development in a shorter period of time.

Particularly, for WEB SUMANDO Project, FE developers are responsables for stylesheets, better said, CSS code. Eventually FE helps in building JavaScript code that back-end developer can’t develop. For this Project, it’s critical that FE developers have a good understandment of CSS and Javascript, but, as it will be demonstrated, FE development require aditional languages and specialized tools for the development of optimized stylesheet, that adds complexity and extends the learning curve.

This documents is intended as an introduction guide to FE development process.
_____
**2.	Tools**
---
There are several tools to help the web development process that range from suggestions to entire librery administrators, whom were built for an specific purpouse in the development cycle.

Particularly, for WEB SUMANDO Project, the FE development process require several tools to help developers achieve a cleaner, quicker and global standarized code. This department have tools to automatize processes to optimize development.

Besides the required tools, developers also need to master the project tools, like Rational Application Developer (RAD) and Websphere. These tools are required since from there, FE developers edit stylesheets, update markup (XUI), add class, change structure and more.

Development process includes optimization, which it’s required to minimize the loading time of an application. Thus, tools like concatenation, minification and code reorganization are includes.

The most importantd and used tools are listed below.

- **LESS**. Language that allows developers to built stylesheets in a shorter period of time.
- **Javascript**. Language to develop the interaction in web. Needed to configure GRUNT tool.
- **NodeJS**. Server required for process automation.
- **NPM**. Package manager of NodeJS
- **GIT**. Versioning control system.
- **GRUNT**. Task manager.
- **Git-revision**. Tool utilized to obtain the Project name branches.
- **Autoprefixer**. Library to add vendors prefixes to stylesheet when required.
- **Concurrent**. Library that allows running several task in parallel.
- **Connect-Proxy**. Allows to use virtual hosts through a local proxy.
- **Clean**. Allows to set rules for folder removal.
- **Connect**. Allows to connect a local server
- **Copy**. Library that allows to copy and move folders.
- **Uglify**. Library that minify and concatenate Javascript code
- **Recess**. Tool that compile, organize, concatenate and minify LESS code and generate stylesheets (CSS).
_____

**3.	FOLDER STRUCTURE**
---
Project folder structure its complex and was built to imitate the structure  of the current themes found in BTT tool. Currently, this Project has two themes that contains the colors for Banorte and Ixe.

Inside FE Project there are two main themes that contains the source files (code, images and fonts), like it’s shown in  Image 1). These files are proccessed and compiled to obtaint the Banorte and Ixe themes.

 ![Folder Structure](https://bitbucket.org/uxdevpublic/images-for-readme/raw/master/image1.jpg)
 
 Image 1. Folder Structure


The folder with the name Banorte it’s the main one and it’s the folder that contains  the source code for both themes, Banorte and Ixe. Inside this folder there are other sub folders that contains files with LESS extensión, which are source code for the stylesheets, as shown in Image 2.

 ![Folder Structure](https://bitbucket.org/uxdevpublic/images-for-readme/raw/master/image2.jpg)
 
 Image 2. Banorte main folder.

Inside Banorte folder (shown in Image 2), there are six folders, whom mainly contains files with main rules inherit by other files. Folders that needs to be in consideration for future development are described below:

**Folder Images**. Contains images and sprites for the web application.
**Carpeta customized**. Contains specialiced files, like buttons, icons and fonts rules (Image 3).

![Customized folder](https://bitbucket.org/uxdevpublic/images-for-readme/raw/master/image3.jpg)
  
Image 3. Customized folder

**Folder microflujos**.  This folder is located inside the customized folder, and contains all the stylesheets of the flows developed. New stylesheets built for new flows should be placed here (Image 4).

![Customized folder](https://bitbucket.org/uxdevpublic/images-for-readme/raw/master/image4.jpg)

Image 4. Microflujos folder

Stylesheets inside Banorte folder are copied into Ixe older, with and exception of the image folder. Images that needs to be included into the ixe theme, should be manually copied into the image folder, it’s shown in Image 5.
 
![Ixe theme folder](https://bitbucket.org/uxdevpublic/images-for-readme/raw/master/image5.jpg)

Image 5. Ixe theme folder




_______
**4.	Theme identification**
----
Inside the customized folder there is a file called configuración.ini which contains a variable that identify the theme to be applied. The variable it’s read during compilation process and it’s critical for the development process. In Image 6, you can see the variable for Banorte theme.
 
![File configuration.ini, for Banorte theme](https://bitbucket.org/uxdevpublic/images-for-readme/raw/master/image6.jpg)
  
Image 6. File configuracion.ini, for Banorte theme.


_____
**5.	Configuration file**
------
The file variable.less its a configuration file located inside the customized folder, whom contains the range of colors for Banorte and Ixe themes. Depending on the theme, the compilation will apply the styles defined for that specific file. Compiling process will read the variable described in the Image 6 and will apply the rules described for that theme.
Several options described for each theme can be seen in Image 7.

 
  ![Compilation process](https://bitbucket.org/uxdevpublic/images-for-readme/raw/master/image7.jpg)
  
Image 7. Variables file
_______
**6.	Compilation process**
----
For compiling stylesheets it’s neccesary to execute a sentence from the terminal. Said sentence will call the task manager (GRUNT) to initalize the process.
The compilation process was built to read the theme variable, read the colors, compile stylesheets and apply several steps that in the end will result something similar to what it’s in the Image 8.
To initialize this process, it’s required to execute the following sentence from the terminal: grunt init
 
  ![Compilation process](https://bitbucket.org/uxdevpublic/images-for-readme/raw/master/image8.jpg)
  
Image 8. Compilation process
___
**7.	Bug fixing process**
-----
This is a complex bug, due the inherit that existis inside the rules of the stylesheets. To adjust a rule it’s important to identify if the changes will be applied to a general rule or to an specific flow.

There are several steps that can help the developer fix a bug, these steps are described below:

 1. Identify the flow where the bug whas found.
 2. Identify the parent rule, so you can track the location of the stylesheet where the rule it’s written.
 3. Define if the rule it’s flow specific or general.
 4. If the fix it’s general, it’s neccesary to change the rule from the stylesheets located inside the folder called customized..
 5. If the fix it’s a flow specific bug, it’s neccesary to identify the parent rule, locate the stylesheet and apply the changes to that specific rule inside of the stylesheet (these stylesheet can be found inside the folder called microflujos).
 6. Once the bug it’s fixed, it’s critical  to test the changes into a local environment, QA and preprodution environment.

______

**8.	New flow process**
-----
The new flow process require the manipulation of XUI files to add class and adjust the markup estructure. For a fast location of stylesheet, the parent class should have a similar name of the flow that it’s currently being developed and a file (LESS file) should be created with the same name as the class.
Built a new flow require not only FE tools, but also the RAD and Websphere tools.
There are several steps that can help the developer to build a new flow. These steps are described below:

 1. Identify the name of the new flow.
 2. Create a new file (LESS file) inside the microflujos folder, explained in Image 4. The name of the file should be the same as the name of the class added into the flow.
 3. Add clases into the XUI and manipulate the markup structure to optimize it. It’s good to remember that a class should be added to the first panel on the XUI. This class should have the same name as the XUI and the file created (LESS file).
 4. Built the rules inside the file created in step two.
 5. Once the new flow and the new rules are finished, it’s critical to test the changes into a local, QA and preproduction environment. 

___
**9.	Conclusion**
-----
During the several sections of this document has been indicated the importance of having a good understandment of the tools included in the development process. Bug fixing and new flows not only require to master the tools, but also the environment. The impact of these modifications can’t be measured without the previosly described and it’s suggested that each new developer have time to understand the environment.

The tools required for this process are used daily and the developer has to know them and master them.














